import Vue from 'vue';
import Router from 'vue-router';
import { List, Info, Add } from './components/views';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: List
    },
    {
      path: '/pizza/:id',
      name: 'pizza',
      component: Info
    },
    {
      path: '/new',
      name: 'new',
      component: Add
    }
  ]
});
