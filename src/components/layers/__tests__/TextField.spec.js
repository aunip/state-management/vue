import '@testing-library/jest-dom/extend-expect';
import { render, cleanup } from '@testing-library/vue';
import { shallowMount } from '@vue/test-utils';
import TextField from '../TextField';

describe('<TextField />', () => {
  afterEach(cleanup);

  it('Should Component Renders', () => {
    const { container } = render(TextField);

    expect(container).toBeDefined();
  });

  it('Should Component Renders Simply', () => {
    const { container, getByText } = render(TextField, {
      scopedSlots: {
        default: '<span>Hello World</span>'
      }
    });

    expect(container.querySelector('p')).toHaveStyle('font-size: 16px');
    // expect(container.querySelector('p').style['font-size']).toEqual('16px');

    expect(getByText('Hello World')).toBeInTheDocument();
  });

  it('Should Component Renders Fully', () => {
    const { getByPlaceholderText } = render(TextField, {
      props: {
        type: 'number',
        placeholder: 'Test',
        value: 42,
        size: 18,
        editable: true
      }
    });

    expect(getByPlaceholderText('Test')).toHaveAttribute('type', 'number');
    expect(getByPlaceholderText('Test')).toHaveStyle('font-size: 18px');
    expect(getByPlaceholderText('Test').value).toEqual('42');
  });

  it('Should "Input" Custom Event Works Well', async () => {
    const wrapper = shallowMount(TextField);

    wrapper.vm.$emit('input', { target: { value: 'Hello World' } });

    await wrapper.vm.$nextTick();

    expect(wrapper.emitted().input).toBeTruthy();

    expect(wrapper.emitted().input).toHaveLength(1);

    expect(wrapper.emitted().input[0]).toEqual([{ target: { value: 'Hello World' } }]);
  });
});
