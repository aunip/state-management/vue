import '@testing-library/jest-dom/extend-expect';
import { render, cleanup } from '@testing-library/vue';
import Row from '../Row';

describe('<Row />', () => {
  afterEach(cleanup);

  it('Should Component Renders', () => {
    const { container } = render(Row);

    expect(container).toBeDefined();
  });

  it('Should Component Renders Simply', () => {
    const { container } = render(Row);

    expect(container.querySelector('.icon')).toBeInTheDocument();
  });

  it('Should Component Renders Fully', () => {
    const { getByText } = render(Row, {
      slots: {
        'left-cell': '<span>Left</span>',
        'right-cell': '<span>Right</span>'
      }
    });

    expect(getByText('Left')).toBeInTheDocument();
    expect(getByText('Right')).toBeInTheDocument();
  });
});
