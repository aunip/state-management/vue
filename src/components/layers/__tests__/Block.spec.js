import '@testing-library/jest-dom/extend-expect';
import { render, cleanup } from '@testing-library/vue';
import Block from '../Block';

describe('<Block />', () => {
  afterEach(cleanup);

  it('Should Component Renders', () => {
    const { container } = render(Block);

    expect(container).toBeDefined();
  });

  it('Should Component Renders Simply', () => {
    const { getByText } = render(Block, {
      scopedSlots: {
        default: '<span>Hello World</span>'
      }
    });

    expect(getByText('Hello World')).toBeInTheDocument();
  });

  it('Should Component Renders Fully', () => {
    const { container } = render(Block, {
      props: {
        height: 100,
        color: '#212121'
      },
      scopedSlots: {
        default: '<span>Hello World</span>'
      }
    });

    const block = container.querySelector('.block');
    const content = container.querySelector('.content');

    expect(block).toHaveStyle('height: 100px');
    // expect(block.style['height']).toEqual('100px');

    expect(content).toHaveStyle('background: #212121');
    // expect(content.style['background']).toEqual('rgb(33, 33, 33)');

    expect(container.querySelector('.frame')).toBeInTheDocument();
  });
});
