import '@testing-library/jest-dom/extend-expect';
import { render, fireEvent, cleanup } from '@testing-library/vue';
import Radio from '../Radio';

describe('<Radio />', () => {
  afterEach(cleanup);

  it('Should Component Renders', () => {
    const { container } = render(Radio);

    expect(container).toBeDefined();
  });

  it('Should Component Renders Fully', () => {
    const { container } = render(Radio, {
      props: {
        color: '#fafafa'
      }
    });

    const icon = container.querySelector('svg');
    const circles = container.querySelectorAll('circle');

    expect(icon).toHaveStyle('cursor: pointer');
    // expect(icon.style['cursor']).toEqual('pointer');

    expect(circles).toHaveLength(2);
  });

  it('Should Click Event Works Well', async () => {
    const handleClick = jest.fn();

    const { container } = render(Radio, {
      props: {
        color: '#fafafa',
        handleClick
      }
    });

    const icon = container.querySelector('svg');

    // First Click
    await fireEvent.click(icon);
    expect(container.querySelectorAll('circle')).toHaveLength(1);

    // Second Click
    await fireEvent.click(icon);
    expect(container.querySelectorAll('circle')).toHaveLength(2);

    expect(handleClick).toHaveBeenCalledTimes(2);
  });
});
