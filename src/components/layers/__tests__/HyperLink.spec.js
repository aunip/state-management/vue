import '@testing-library/jest-dom/extend-expect';
import { render, cleanup } from '@testing-library/vue';
import { shallowMount } from '@vue/test-utils';
import HyperLink from '../HyperLink';

describe('<HyperLink />', () => {
  afterEach(cleanup);

  it('Should Component Renders', () => {
    const { container } = render(HyperLink);

    expect(container).toBeDefined();
  });

  it('Should Component Renders Simply', () => {
    const { getByText } = render(HyperLink, {
      scopedSlots: {
        default: '<span>Click Me</span>'
      }
    });

    expect(getByText('Click Me')).toBeInTheDocument();
  });

  it('Should Component Renders Fully', () => {
    const { container } = render(HyperLink, {
      props: {
        to: '#test',
        size: 18
      },
      scopedSlots: {
        default: '<span>Click Me</span>'
      }
    });

    const hyperlink = container.querySelector('a');

    expect(hyperlink).toHaveAttribute('href', '#test');

    expect(hyperlink).toHaveStyle('font-size: 18px');
    // expect(hyperlink.style['font-size']).toEqual('18px');
  });

  it('Should "Click" Custom Event Works Well', async () => {
    const wrapper = shallowMount(HyperLink);

    wrapper.vm.$emit('click');

    await wrapper.vm.$nextTick();

    expect(wrapper.emitted().click).toBeTruthy();

    expect(wrapper.emitted().click).toHaveLength(1);
  });
});
