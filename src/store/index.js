import Vue from 'vue';
import Vuex, { Store } from 'vuex';
import { getters } from './getters';
import { actions } from './actions';
import { mutations } from './mutations';

const STORE = 'AUNIP';

Vue.use(Vuex);

const storagePlugin = store => {
  store.subscribe((_, state) => {
    sessionStorage.setItem(STORE, JSON.stringify(state));
  });
};

const state = {
  pizzas: []
};

const store = {
  state,
  getters,
  actions,
  mutations: {
    ...mutations,
    initStore(state) {
      if (sessionStorage.getItem(STORE)) {
        this.replaceState(Object.assign(state, JSON.parse(sessionStorage.getItem(STORE))));
      }
    }
  },
  plugins: [storagePlugin]
};

export default new Store(store);
