export const getters = {
  getPizzas(state) {
    return state.pizzas || [];
  },
  getPizzaById(state) {
    // const pizzas = getters.getPizzas;
    const pizzas = state.pizzas || [];

    return id => {
      if (pizzas.length > 0) {
        return pizzas.find(pizza => pizza.id === id);
      }

      return null;
    };
  }
};
