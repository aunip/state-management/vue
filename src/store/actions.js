import { SET_PIZZAS, RESET_PIZZAS, ADD_PIZZA, SET_PIZZA, DEL_PIZZA } from './constants';

export const actions = {
  setPizzas({ commit }, pizzas) {
    const payload = pizzas;

    commit(SET_PIZZAS, payload);
  },
  resetPizzas({ commit }) {
    commit(RESET_PIZZAS);
  },
  addPizza({ commit }, pizza) {
    const payload = pizza;

    commit(ADD_PIZZA, payload);
  },
  setPizza({ commit }, pizza) {
    const payload = pizza;

    commit(SET_PIZZA, payload);
  },
  delPizza({ commit }, id) {
    const payload = id;

    commit(DEL_PIZZA, payload);
  }
};
