import { SET_PIZZAS, RESET_PIZZAS, ADD_PIZZA, SET_PIZZA, DEL_PIZZA } from './constants';
import { generateId } from '../utils';

export const mutations = {
  [SET_PIZZAS](state, payload) {
    state.pizzas = payload;
  },
  [RESET_PIZZAS](state) {
    state.pizzas = [];
  },
  [ADD_PIZZA](state, payload) {
    state.pizzas = [
      ...state.pizzas,
      {
        id: generateId(),
        ...payload
      }
    ];
  },
  [SET_PIZZA](state, payload) {
    state.pizzas = state.pizzas.map(pizza => (pizza.id === payload.id ? { ...pizza, ...payload } : pizza));
  },
  [DEL_PIZZA](state, payload) {
    state.pizzas = state.pizzas.filter(({ id }) => id !== payload);
  }
};
