import { mutations } from '../mutations';
import * as Constants from '../constants';

describe('Mutations', () => {
  const initialState = [];

  it('SET_PIZZAS', () => {
    const state = {
      pizzas: []
    };

    const payload = [
      {
        id: 'uguj2keowpq',
        label: '4 Fromages',
        items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      }
    ];

    mutations[Constants.SET_PIZZAS](state, payload);

    expect(state.pizzas).toEqual(payload);
  });

  it('RESET_PIZZAS', () => {
    const state = {
      pizzas: [
        {
          id: 'uguj2keowpq',
          label: '4 Fromages',
          items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        }
      ]
    };

    mutations[Constants.RESET_PIZZAS](state);

    expect(state.pizzas).toEqual(initialState);
  });

  it('ADD_PIZZA', () => {
    const state = {
      pizzas: []
    };

    const payload = {
      label: '4 Fromages',
      items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
      price: 14.9
    };

    mutations[Constants.ADD_PIZZA](state, payload);

    expect(state.pizzas).toHaveLength(initialState.length + 1);
  });

  it('SET_PIZZA', () => {
    const state = {
      pizzas: [
        {
          id: 'uguj2keowpq',
          label: '4 Fromages',
          items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        },
        {
          id: 'u91diii1em',
          label: 'Calzone',
          items: ['Mozzarella', 'Jambon Blanc', 'Champignons', 'Emmental', 'Oeuf', 'Origan'],
          price: 13.9
        }
      ]
    };

    const payload = {
      id: 'uguj2keowpq',
      label: '3 Fromages',
      items: ['Mozzarella', 'Gouda', 'Reblochon', 'Gorgonzola'],
      price: 13.9
    };

    mutations[Constants.SET_PIZZA](state, payload);

    expect(state.pizzas).toEqual(state.pizzas.map(value => (value.id === payload.id ? payload : value)));
  });

  it('DEL_PIZZA', () => {
    const state = {
      pizzas: [
        {
          id: 'uguj2keowpq',
          label: '4 Fromages',
          items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        }
      ]
    };

    mutations[Constants.DEL_PIZZA](state, 'uguj2keowpq');

    expect(state.pizzas).toEqual(initialState);
  });
});
