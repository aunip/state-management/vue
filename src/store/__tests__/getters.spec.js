import { getters } from '../getters';

describe('Getters', () => {
  const state = {
    pizzas: [
      {
        id: 'uguj2keowpq',
        label: '4 Fromages',
        items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      }
    ]
  };

  it('getPizzas', () => {
    expect(getters.getPizzas({})).toHaveLength(0);
    expect(getters.getPizzas(state)).toHaveLength(1);
  });

  it('getPizzaById', () => {
    expect(getters.getPizzaById({})()).toBeNull();
    expect(getters.getPizzaById(state)('uguj2keowpq')).toEqual({
      id: 'uguj2keowpq',
      label: '4 Fromages',
      items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
      price: 14.9
    });
  });
});
