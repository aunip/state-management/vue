import { actions } from '../actions';
import * as Constants from '../constants';

describe('Actions', () => {
  it('setPizzas', () => {
    const commit = jest.fn();

    const pizzas = [
      {
        id: 'uguj2keowpq',
        label: '4 Fromages',
        items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      }
    ];

    actions.setPizzas({ commit }, pizzas);

    expect(commit).toHaveBeenCalledWith(Constants.SET_PIZZAS, pizzas);
  });

  it('resetPizzas', () => {
    const commit = jest.fn();

    actions.resetPizzas({ commit });

    expect(commit).toHaveBeenCalledWith(Constants.RESET_PIZZAS);
  });

  it('addPizza', () => {
    const commit = jest.fn();

    const pizza = {
      label: '4 Fromages',
      items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
      price: 14.9
    };

    actions.addPizza({ commit }, pizza);

    expect(commit).toHaveBeenCalledWith(Constants.ADD_PIZZA, pizza);
  });

  it('setPizza', () => {
    const commit = jest.fn();

    const pizza = {
      id: 'uguj2keowpq',
      label: '4 Fromages',
      items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
      price: 14.9
    };

    actions.setPizza({ commit }, pizza);

    expect(commit).toHaveBeenCalledWith(Constants.SET_PIZZA, pizza);
  });

  it('delPizza', () => {
    const commit = jest.fn();

    actions.delPizza({ commit }, 'uguj2keowpq');

    expect(commit).toHaveBeenCalledWith(Constants.DEL_PIZZA, 'uguj2keowpq');
  });
});
