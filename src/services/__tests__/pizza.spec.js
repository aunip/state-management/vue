import { readAllPizzas } from '../pizza';

describe('Pizzas Service', () => {
  it('readAllPizzas', async () => {
    const pizzas = await readAllPizzas();

    expect(pizzas).toHaveLength(25);
  });
});
